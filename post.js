import React from 'react';
import { StyleSheet, View,Text, TextInput, Button, AsyncStorage } from 'react-native';

export default class Post extends React.Component{
	state = {
		isEdit: false,
		text: this.props.todo.text,
	}

	updateToTodo = (id) => {
		this.props.updateTodo({id, text: this.state.text});
		this.setState({
			isEdit: false
		})
		
	}	
	render(){
		const {todo} = this.props;
		return(
			<View>
				{!this.state.isEdit?
					<View>
						<Text>{todo.text}</Text>
						<Button title='Delete' onPress={()=> {this.props.deleteTodo(todo.id)}}></Button>
						<Button title='Update' onPress={()=>(this.setState({isEdit: true}))}></Button>
					</View>
					: 
					<View>
						<TextInput value ={this.state.text} onChangeText={(text)=>this.setState({text})}/>
						<Button title='Done' onPress={()=>this.updateToTodo(todo.id)}/>
					</View>
				}
			</View>
			)
	}
}
