import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity } from 'react-native';
import { getTodoList, deleteTodo, updateTodo } from './actions/todo';
import Post from './post';

class Display extends React.Component {
  componentDidMount() {
    this.props.getTodoList();
  }

  render() {
    if (!this.props.list.length) return null;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ borderWidth: 5, width: '100%', padding: '4%' }}>
          {this.props.list.map((todo, index) => (
            <Post key={todo.id} todo={todo} updateTodo={this.props.updateTodo} deleteTodo={this.props.deleteTodo} />
          ))}
        </View>
      </View>
    );
  }
}

const mapDisptachToProps = {
  getTodoList,
  deleteTodo,
  updateTodo
};

const mapStateToProps = ({ todo }) => ({ list: todo.list });

export default connect(mapStateToProps, mapDisptachToProps)(Display);
