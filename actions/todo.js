import { AsyncStorage } from 'react-native';

export const addTodo = todo => ({
  type: 'add todo',
  payload: todo
});

export const deleteTodo = index => ({
  type: 'delete todo',
  payload: index
});

export const updateTodo = newTodo => ({
  type: 'update todo',
  payload: newTodo
});

export const getTodoList = () => dispatch => {
  return AsyncStorage.getItem('TODO').then(list => {
    if (list) {
      return dispatch({
        type: 'get todo',
        payload: list
      });
    }
  });
};
