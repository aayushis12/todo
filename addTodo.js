import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, TextInput, Button, AsyncStorage } from 'react-native';
import Display from './display';
import { addTodo } from './actions/todo';

class AddTodo extends React.Component {
  state = {
    text: 'Enter text here'
  };

  addTodo = () => {
    const todo = { id: Math.random(), text: this.state.text };
    this.props.addTodo(todo);
    this.setState({
      text: ''
    });

    // get asycn/ then update
    // send to asyn update

    // state

    AsyncStorage.getItem('TODO').then((list = []) => {
      const newList = list && Object.assign([], list, todo);
      AsyncStorage.setItem('TODO', newList);
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.inputBoxStyle}
          onChangeText={text => this.setState({ text })}
          value={this.state.text}
        />
        <Button title="Add" onPress={() => this.addTodo()} />
        <Display />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: '14%',
    alignItems: 'center'
  },
  inputBoxStyle: {
    height: 40,
    width: '80%',
    borderColor: 'gray',
    borderWidth: 1
  }
});

const mapDispatchToProps = {
  addTodo
};

export default connect(null, mapDispatchToProps)(AddTodo);
