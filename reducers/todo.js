const initialState = {
  list: [],
  id: 0
};
export default (state = initialState, action) => {
  switch (action.type) {
    case 'add todo':
      return {
        ...state,
        list: [...state.list, action.payload],
        id: action.payload.id
      };
    case 'get todo':
      return {
        ...state,
        list: [...state.list, action.payload],
        id: ''
      };
    case 'delete todo':
      return {
        ...state,
        list: state.list.filter(task => task.id !== action.payload)
      };
    case 'update todo':
      const newList = state.list.map(todo => {
        if (todo.id == action.payload.id) {
          todo.text = action.payload.text;
        }
        return todo;
      });
      return {
        ...state,
        list: newList
      };
    default:
      return state;
  }
};
